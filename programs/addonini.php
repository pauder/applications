; <?php /*
[general]
name                            ="applications"
version                         ="4.7.0"
addon_type                      ="EXTENSION"
encoding                        ="UTF-8"
mysql_character_set_database    ="latin1,utf8"
description                     ="Links managements addon with groups access"
description.fr                  ="Module de gestion de liens avec des droits d'accès par groupes"
long_description.fr             ="README.md"
db_prefix                       ="applications_"
delete                          ="1"
ov_version                      ="8.6.97"
author                          ="paul de rosanbo ( paul.derosanbo@cantico.fr )"
icon                            ="preferences-system-network-sharing.png"
addon_access_control            ="0"
configuration_page              ="configuration"
tags							="extension,default"
; */ ?>
