<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';


/*
 */

/**
 * Management of OVML containers.
 *
 * There are 2 containers :
 * <OCAddon name="applications" actions="applications">
 * and
 * <OCAddon name="applications" actions="application" applicationid="n">
 *
 * They both return the following variables.
 *
 * <OVApplicationId>
 * <OVApplicationUrl> : The url to use in links (may be modified depending on configuration).
 * <OVApplicationDirectUrl> : The original url to the destination.
 * <OVApplicationName>
 * <OVApplicationLongName>
 * <OVApplicationDescription>
 * <OVApplicationPlace> : 1, 2, 3 or 4.
 * <OVApplicationPlaceText> : 'In specific section', 'In user section', 'No place' or 'In sitemap'.
 * <OVApplicationDisplayMode> : 1, 2, 3 or 4.
 * <OVApplicationDisplayModeText> : 'New window', 'Same window', 'In Ovidentia body'.
 *
 * @param Array $args
 * @return Array
 */
function applications_ovml($args)
{
    global $babDB;

    if (!isset($args['action'])) {
        return array();
    }

    switch (strtolower($args['action'])) {

    	case 'applications':
    	     
    	    return applications_ovml_applications($args);

    	    	
    	case 'application':

    	    return applications_ovml_application($args);

    }

    return array();
}

/**
 * @param array $args
 * @return array
 */
function applications_ovml_applications($args)
{
    global $babDB;

    $addon = bab_getAddonInfosInstance('applications');

    $sql = 'SELECT id, url, shortdesc, longdesc, description, display_mode, place FROM applications_list';

    /* Verifie si des filtres sont demandes */
    $where = array();
    if (isset($args['place'])) {
        switch($args['place']) {
        	case '1':
        	    $where[] = "place='1'";
        	    break;
        	case '2':
        	    $where[] = "place='2'";
        	    break;
        	case '3':
        	    $where[] = "place='3'";
        	    break;
        }
    }
   
    if (isset($args['displaymode'])) {
        switch($args['displaymode']) {
        	case '1':
                $where[] = "display_mode='new_window'";
        	    break;
        	case '2':
        	    $where[] = "display_mode='body'";
        	    break;
        	case '3':
        	    $where[] = "display_mode='iframe'";
        	    break;
        }
    }
    if (!empty($where)) {
        $sql .= implode(' AND ', $where);
    }
    $sql .= ' ORDER BY shortdesc ASC'; /* ordre alphabetique sur le nom */


    $apps = $babDB->db_query($sql);
  
    while ($app = $babDB->db_fetch_assoc($apps)) {
        /* Creation des variables OVML du container */
        $tab = array();
        /* Test des droits d'acces */
        if (bab_isAccessValid('applications_groups', $app['id'])) {
            $tab['ApplicationId'] = $app['id'];
            if ($app['display_mode'] == 'iframe') {
                $tab['ApplicationUrl'] = $addon->getUrl().'main&amp;idx=iframe&amp;id_app='.$app['id'];
            } else {
                $tab['ApplicationUrl'] = applications_getUrlAccordingToConfiguration($app['url']);
            }
            $tab['ApplicationDirectUrl'] = $app['url'];
            $tab['ApplicationName'] = $app['shortdesc'];
            $tab['ApplicationLongName'] = $app['longdesc'];
            $tab['ApplicationDescription'] = $app['description'];
            $tab['ApplicationPlace'] = $app['place'];
            switch($app['place']) {
            	case '1':
            	    $tab['ApplicationPlaceText'] = applications_translate('In specific section');
            	    break;
            	case '2':
            	    $tab['ApplicationPlaceText'] = applications_translate('In user section');
            	    break;
            	case '3':
            	    $tab['ApplicationPlaceText'] = applications_translate('No place');
            	    break;
            	case '4':
            	    $tab['ApplicationPlaceText'] = applications_translate('In sitemap');
            	    break;
            	default:
            	    $tab['ApplicationPlaceText'] = '';
            	    break;
            }
            switch($app['display_mode']) {
            	case 'new_window':
            	    $tab['ApplicationDisplayMode'] = '1';
            	    $tab['ApplicationDisplayModeText'] = applications_translate('New window');
            	    break;
            	case 'body':
            	    $tab['ApplicationDisplayMode'] = '2';
            	    $tab['ApplicationDisplayModeText'] = applications_translate('Same window');
            	    break;
            	case 'iframe':
            	    $tab['ApplicationDisplayMode'] = '3';
            	    $tab['ApplicationDisplayModeText'] = applications_translate('In Ovidentia body');
            	    break;
            	default:
            	    $tab['ApplicationDisplayMode'] = '';
            	    $tab['ApplicationDisplayModeText'] = '';
            	    break;
            }
            $res[] = $tab;
        }
    }

    return $res;
}




/**
 * @param array $args
 * @return array
 */
function applications_ovml_application($args)
{
    global $babDB;

    $addon = bab_getAddonInfosInstance('applications');

    /* Verifie l'identifiant de l'application et les droits d'acces */
    if (!isset($args['applicationid'])) {
        return array();
    }
  
    $applicationid = $args['applicationid'];
    $sql = 'SELECT id, url, shortdesc, longdesc, description, display_mode, place FROM applications_list WHERE id = ' . $babDB->quote($applicationid);

    $apps = $babDB->db_query($sql);
    $app = $babDB->db_fetch_assoc($apps);
    if (!$app) {
        return array();
    }
 
    /* Creation des variables OVML du container */
    $tab = array();
    /* Test des droits d'acces */
    if (bab_isAccessValid('applications_groups', $app['id'])) {
        $tab['ApplicationId'] = $app['id'];
        if ($app['display_mode'] == 'iframe') {
            $tab['ApplicationUrl'] = $addon->getUrl().'main&amp;idx=iframe&amp;id_app='.$app['id'];
        } else {
            $tab['ApplicationUrl'] = applications_getUrlAccordingToConfiguration($app['url']);
        }
        $tab['ApplicationDirectUrl'] = $app['url'];
        $tab['ApplicationName'] = $app['shortdesc'];
        $tab['ApplicationLongName'] = $app['longdesc'];
        $tab['ApplicationDescription'] = $app['description'];
        $tab['ApplicationPlace'] = $app['place'];
        switch($app['place']) {
        	case '1':
        	    $tab['ApplicationPlaceText'] = applications_translate('In specific section');
        	    break;
        	case '2':
        	    $tab['ApplicationPlaceText'] = applications_translate('In user section');
        	    break;
        	case '3':
        	    $tab['ApplicationPlaceText'] = applications_translate('No place');
        	    break;
    	    case '4':
    	        $tab['ApplicationPlaceText'] = applications_translate('In sitemap');
    	        break;
        	default:
        	    $tab['ApplicationPlaceText'] = '';
        	    break;
        }
        switch($app['display_mode']) {
        	case 'new_window':
        	    $tab['ApplicationDisplayMode'] = '1';
        	    $tab['ApplicationDisplayModeText'] = applications_translate('New window');
        	    break;
        	case 'body':
        	    $tab['ApplicationDisplayMode'] = '2';
        	    $tab['ApplicationDisplayModeText'] = applications_translate('Same window');
        	    break;
        	case 'iframe':
        	    $tab['ApplicationDisplayMode'] = '3';
        	    $tab['ApplicationDisplayModeText'] = applications_translate('In Ovidentia body');
        	    break;
        	default:
        	    $tab['ApplicationDisplayMode'] = '';
        	    $tab['ApplicationDisplayModeText'] = '';
        	    break;
        }
        $res[] = $tab;
    }

    return $res;
}
